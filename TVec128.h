#ifndef TVEC128_H
#define TVEC128_H

#include <inttypes.h>
#ifdef _EE
#include "ee_mmi.h"
#else
typedef unsigned __int128 uint128_t;
#endif

template <typename T, int p>
class TVec128 {
public:
    // Contructors
	TVec128() {}
    TVec128(const TVec128& v) { gpr = v.gpr; }
    TVec128(const T* v) { gpr = *(const uint128_t *)v; }
    TVec128(const T& s) { for (int i = 0; i < p; i++) e[i] = s; }

    // Compound Assignment Operators
	TVec128& operator+=(const TVec128& v) { for (int i = 0; i < p; i++) e[i] += v.e[i]; return (*this);	}
    TVec128& operator-=(const TVec128& v) { for (int i = 0; i < p; i++) e[i] += v.e[i]; return (*this); }
    TVec128& operator*=(const TVec128& v) { for (int i = 0; i < p; i++) e[i] *= v.e[i]; return (*this); }
    TVec128& operator/=(const TVec128& v) { for (int i = 0; i < p; i++) e[i] /= v.e[i]; return (*this); }
    TVec128& operator>>=(const T& s) { for (int i = 0; i < p; i++) e[i] >>= s; return (*this); }
    TVec128& operator<<=(const T& s) { for (int i = 0; i < p; i++) e[i] <<= s; return (*this); }

	// Binary Arithmetic Operators
    TVec128 operator+(const TVec128& v) const { return (TVec128(*this) += v); }
    TVec128 operator-(const TVec128& v) const { return (TVec128(*this) -= v); }
    TVec128 operator*(const TVec128& v) const { return (TVec128(*this) *= v); }
    TVec128 operator/(const TVec128& v) const { return (TVec128(*this) /= v); }
    TVec128 operator>>(const T& s) const { return (TVec128(*this) >>= s); }
    TVec128 operator<<(const T& s) const { return (TVec128(*this) <<= s); }

public:
    union {
        uint128_t gpr;
        T e[p];
    };
} __attribute__((aligned(16)));

// Pre-defined signed int classes
typedef TVec128<int8_t, 16> CInt8_16;
typedef TVec128<int16_t, 8> CInt16_8;
typedef TVec128<int32_t, 4> CInt32_4;
typedef TVec128<int64_t, 2> CInt64_2;
// Pre-defined unsigned int classes
typedef TVec128<uint8_t, 16> CUInt8_16;
typedef TVec128<uint16_t, 8> CUInt16_8;
typedef TVec128<uint32_t, 4> CUInt32_4;
typedef TVec128<uint64_t, 2> CUInt64_2;

#ifdef _EE
// Template specialization for CInt8_16
template<>
inline CInt8_16& CInt8_16::operator+=(const CInt8_16& v) {
	gpr = _mmi_paddsb(gpr, v.gpr);
	return (*this);
}

template<>
inline CInt8_16& CInt8_16::operator-=(const CInt8_16& v) {
	gpr = _mmi_psubsb(gpr, v.gpr);
	return (*this);
}

// Template specialization for CInt16_8
template<>
inline CInt16_8& CInt16_8::operator+=(const CInt16_8& v) {
	gpr = _mmi_paddsh(gpr, v.gpr);
	return (*this);
}

template<>
inline CInt16_8& CInt16_8::operator-=(const CInt16_8& v) {
	gpr = _mmi_psubsh(gpr, v.gpr);
	return (*this);
}

// Template specialization for CInt32_4
template<>
inline CInt32_4& CInt32_4::operator+=(const CInt32_4& v) {
	gpr = _mmi_paddsw(gpr, v.gpr);
	return (*this);
}

template<>
inline CInt32_4& CInt32_4::operator-=(const CInt32_4& v) {
	gpr = _mmi_psubsw(gpr, v.gpr);
	return (*this);
}

// Template specialization for CUInt8_16
template<>
inline CUInt8_16& CUInt8_16::operator+=(const CUInt8_16& v) {
	gpr = _mmi_paddub(gpr, v.gpr);
	return (*this);
}

template<>
inline CUInt8_16& CUInt8_16::operator-=(const CUInt8_16& v) {
	gpr = _mmi_psubub(gpr, v.gpr);
	return (*this);
}

// Template specialization for CUInt16_8
//
template<>
inline CUInt16_8& CUInt16_8::operator+=(const CUInt16_8& v) {
	gpr = _mmi_padduh(gpr, v.gpr);
	return (*this);
}

template<>
inline CUInt16_8& CUInt16_8::operator-=(const CUInt16_8& v) {
	gpr = _mmi_psubuh(gpr, v.gpr);
	return (*this);
}

// Template specialization for CUInt32_4
template<>
inline CUInt32_4& CUInt32_4::operator+=(const CUInt32_4& v) {
	gpr = _mmi_padduw(gpr, v.gpr);
	return (*this);
}

template<>
inline CUInt32_4& CUInt32_4::operator-=(const CUInt32_4& v) {
	gpr = _mmi_psubuw(gpr, v.gpr);
	return (*this);
}
#endif

#endif
