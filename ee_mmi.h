#ifndef EE_MMI_H
#define EE_MMI_H

#include <inttypes.h>

typedef unsigned int uint128_t __attribute__((mode(TI)));
typedef uint128_t int8x16_t;
typedef uint128_t int16x8_t;
typedef uint128_t int32x4_t;
typedef uint128_t int64x2_t;
typedef uint128_t uint8x16_t;
typedef uint128_t uint16x8_t;
typedef uint128_t uint32x4_t;
typedef uint128_t uint64x2_t;

//---------------------------------------------------------------------------
static inline int8x16_t _mmi_paddsb(int8x16_t vec1, int8x16_t vec2)
{
    int8x16_t rv;

    // PADDSB : Parallel Add with Signed Saturation Byte
    asm("paddsb %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int16x8_t _mmi_paddsh(int16x8_t vec1, int16x8_t vec2)
{
    int16x8_t rv;

    // PADDSH : Parallel Add with Signed Saturation Halfword
    asm("paddsh %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_paddsw(int32x4_t vec1, int32x4_t vec2)
{
    int32x4_t rv;

    // PADDSW : Parallel Add with Signed Saturation Word
    asm("paddsw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint8x16_t _mmi_paddub(uint8x16_t vec1, uint8x16_t vec2)
{
    uint8x16_t rv;

    // PADDUB : Parallel Add with Unsigned Saturation Byte
    asm("paddub %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint16x8_t _mmi_padduh(uint16x8_t vec1, uint16x8_t vec2)
{
    uint16x8_t rv;

    // PADDUH : Parallel Add with Unsigned Saturation Halfword
    asm("padduh %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint32x4_t _mmi_padduw(uint32x4_t vec1, uint32x4_t vec2)
{
    uint32x4_t rv;

    // PADDUW : Parallel Add with Unsigned Saturation Word
    asm("padduw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_pextlw(int32x4_t vec1, int32x4_t vec2)
{
    int32x4_t rv;

    // PEXTLW : Parallel Extend Lower from Word
    asm("pextlw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_pextuw(int32x4_t vec1, int32x4_t vec2)
{
    int32x4_t rv;

    // PEXTUW : Parallel Extend Upper from Word
    asm("pextuw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_pmulth(int16x8_t vec1, int16x8_t vec2)
{
    int32x4_t rv;

    // PMULTH : Parallel Multiply Halfword
    asm("pmulth %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int64x2_t _mmi_pmultw(int32x4_t vec1, int32x4_t vec2)
{
    int64x2_t rv;

    // PMULTW : Parallel Multiply Word
    asm("pmultw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_ppacw(int64x2_t vec1, int64x2_t vec2)
{
    int32x4_t rv;

    // PPACW : Parallel Pack to Word
    asm("ppacw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int8x16_t _mmi_psubsb(int8x16_t vec1, int8x16_t vec2)
{
    int8x16_t rv;

    // PSUBSB : Parallel Subtract with Signed Saturation Byte
    asm("psubsb %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int16x8_t _mmi_psubsh(int16x8_t vec1, int16x8_t vec2)
{
    int16x8_t rv;

    // PSUBSH : Parallel Subtract with Signed Saturation Halfword
    asm("psubsh %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline int32x4_t _mmi_psubsw(int32x4_t vec1, int32x4_t vec2)
{
    int32x4_t rv;

    // PSUBSW : Parallel Subtract with Signed Saturation Word
    asm("psubsw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint8x16_t _mmi_psubub(uint8x16_t vec1, uint8x16_t vec2)
{
    uint8x16_t rv;

    // PSUBUB : Parallel Subtract with Unsigned Saturation Byte
    asm("psubub %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint16x8_t _mmi_psubuh(uint16x8_t vec1, uint16x8_t vec2)
{
    uint16x8_t rv;

    // PSUBUH : Parallel Subtract with Unsigned Saturation Halfword
    asm("psubuh %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

//---------------------------------------------------------------------------
static inline uint32x4_t _mmi_psubuw(uint32x4_t vec1, uint32x4_t vec2)
{
    uint32x4_t rv;

    // PSUBUW : Parallel Subtract with Unsigned Saturation Word
    asm("psubuw %[rv], %[vec1], %[vec2] \n"
        : [rv] "=&r"(rv)
        : [vec1] "r"(vec1), [vec2] "r"(vec2));

    return rv;
}

#endif
